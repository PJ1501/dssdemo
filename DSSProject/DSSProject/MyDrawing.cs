﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DSSProject
{
    class MyDrawing
    {
        public Panel view { get; set; }
        public TreeNode rootNode { get; set; }
        public Graphics graphicView { get; set; }
        private int width = 40;
        private int height = 40;
        private int marginLR = 40;
        private int marginTB = 60;
        private int maxWidth = 0;
        private int maxHeight = 0;
        public MyDrawing(Graphics graphic, TreeNode node)
        {
            this.rootNode = node;
            this.graphicView = graphic;
        }
        public MyDrawing(Panel graphic, TreeNode node)
        {
            this.rootNode = node;
            this.view = graphic;
        }
        public void DrawnTree()
        {
            maxWidth = (rootNode.listLevel.Last().Value * width) + (rootNode.listLevel.Last().Value - 1) * marginLR;
            maxHeight = (rootNode.listLevel.Count * height) + ((rootNode.listLevel.Count - 1) * marginTB);

            Panel p = new Panel();
            p.Width = maxWidth + marginLR;
            p.Height = maxHeight + marginTB;
            p.BackColor = Color.Aqua;
            p.Paint += P_Paint;
            view.Controls.Add(p);
        }

        private void P_Paint(object sender, PaintEventArgs e)
        {
            Panel p = sender as Panel;
            Graphics graphicView = p.CreateGraphics();
            graphicView.Clear(Color.White);
            List<string> treeQueue = new List<string>();
            treeQueue.Add(rootNode.nodeID);
            int levelNodes = 0;
            int currentPos = 1;
            int lastX = 0;
            int x = 0; int y = 0;
            while (treeQueue.Count > 0)
            {
                TreeNode node = rootNode.listNodes[treeQueue[0]];

                if (levelNodes == 0 || currentPos == levelNodes)
                {
                    levelNodes = rootNode.listLevel[node.Level];
                    currentPos = 1;
                    x = (maxWidth / rootNode.listLevel[node.Level]) / 2;
                    lastX = x;
                    y = (node.Level - 1) * (height + marginTB);
                }
                else
                {
                    lastX += 2 * x;
                    currentPos++;

                }
                node.NodePosition = new Point(lastX - width / 2, y);
                DrawNode(node,graphicView);
                foreach (TreeNode item in node.ChildNodes)
                {
                    treeQueue.Add(item.nodeID);
                }
                treeQueue.RemoveAt(0);
            }
        }
        private void DrawNode(Graphics graphic, int x, int y, Point parent, int nodeType)
        {
            Rectangle rect = new Rectangle(x, y, width, height);
            if (nodeType == (int)define.nodeType.Decision)
            {
                graphic.DrawRectangle(Pens.Black, rect);
            }
            else if (nodeType == (int)define.nodeType.Event)
            {
                graphic.DrawEllipse(Pens.Black,rect);
            }else
            {
                
            }
            Point child = new Point(x + width / 2, y);
            parent.X = parent.X + width / 2;
            parent.Y = parent.Y + height;
            graphic.DrawLine(Pens.Black, child, parent);
        }
        private void DrawNode(Graphics graphic, int x, int y, int nodeType)
        {
            Rectangle rect = new Rectangle(x, y, width, height);
            if (nodeType == (int)define.nodeType.Decision)
            {
                graphic.DrawRectangle(Pens.Black, rect);
            }
            else if (nodeType == (int)define.nodeType.Event)
            {
                graphic.DrawEllipse(Pens.Black, rect);
            }
        }
        private void DrawNode(TreeNode node, Graphics graphic)
        {
            Rectangle rect = new Rectangle(node.NodePosition.X, node.NodePosition.Y, width, height);
            if (node.nodeType == (int)define.nodeType.Decision)
            {
                graphic.DrawRectangle(Pens.Black, rect);
            }else if(node.nodeType == (int)define.nodeType.Event)
            {
                graphic.DrawEllipse(Pens.Black, rect);
            }else
            {
                // Create string to draw.
                String drawString = node.PayOffValue.ToString();

                // Create font and brush.
                Font drawFont = new Font("Arial", 16);
                SolidBrush drawBrush = new SolidBrush(Color.Black);
                
                // Set format of string.
                StringFormat drawFormat = new StringFormat();
                drawFormat.FormatFlags = StringFormatFlags.DirectionVertical;
                graphic.DrawString(drawString, drawFont, drawBrush, node.NodePosition);
            }
            if (node.Parent != null) {
                Point child = new Point(node.NodePosition.X + width / 2, node.NodePosition.Y);
                Point parent = node.Parent.NodePosition;
                parent.X = parent.X + width / 2;
                parent.Y = parent.Y + height;
                graphic.DrawLine(Pens.Black, child, parent);
                if (node.RelationLabel!=null)
                {
                    String drawString = node.RelationLabel.ToString();

                    // Create font and brush.
                    Font drawFont = new Font("Arial", 7);
                    SolidBrush drawBrush = new SolidBrush(Color.Blue);

                    // Set format of string.
                    StringFormat drawFormat = new StringFormat();
                    drawFormat.FormatFlags = StringFormatFlags.DirectionVertical;
                    graphic.DrawString(drawString,drawFont,drawBrush,node.NodePosition.X,node.NodePosition.Y - 30);
                }                
            }
            if (node.nodeName != null)
            {
                String drawString = node.nodeName.ToString();

                // Create font and brush.
                Font drawFont = new Font("Arial", 7);
                SolidBrush drawBrush = new SolidBrush(Color.Red);

                // Set format of string.
                StringFormat drawFormat = new StringFormat();
                drawFormat.FormatFlags = StringFormatFlags.DirectionVertical;
                graphic.DrawString(drawString, drawFont, drawBrush, node.NodePosition.X, node.NodePosition.Y + height / 2);
            }

        }
    }
}
