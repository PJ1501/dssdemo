﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSSProject
{
    class TreeNode
    {
        //private propeties
        int level = 0;
        private int index = 0;
        public int Level { get; set; }
        public string nodeID { get; set; }
        public string RelationLabel
        {
            get;
            set;
        }
        public Point NodePosition { get; set; }
        public string Description { get; set; }
        private Dictionary<int,int> _listLevel = new Dictionary<int,int>();
        public Dictionary<int,int> listLevel { get {return _listLevel; } set {_listLevel = value; } }
        public List<double> Condition { get; set; }
        public List<double> Prior { get; set; }
        public List<double> Joint { get; set; }
        public List<double> Posterior { get; set; }
        public double PayOffValue { get; set; }
        public double EMV { get; set; }
        public TreeNode EMVNode { get; set; }
        public string EMVDescription { get; set; }
        private List<double> _MaxEventPayOff = new List<double>();
        public List<double> MaxEventPayOff { get { return _MaxEventPayOff; } set { _MaxEventPayOff = value; } }
        private TreeNode parent;
        private List<TreeNode> childNodes = new List<TreeNode> { };
        private List<double> _payOffState = new List<double>();
        public List<double> payOffState
        {
            get
            {
                return _payOffState;
            }
            set
            {
                this._payOffState = value;
            }
        }
        public List<double> priorState { get; set; }

        //public propeties
        public List<TreeNode> ChildNodes
        {
            get
            {
                return childNodes;
            }
            set
            {
                childNodes = value;
            }
        }

        public int nodeType { get; set; }
        public String nodeLabel { get; set; }
        public String nodeName { get; set; }


        public TreeNode Parent
        {
            get
            {
                return parent;
            }
            set
            {
                parent = value;
                this.level = parent.getLevel() + 1;
            }
        }

        public int getLevel() { return this.level; }


        //Constructor
        public TreeNode(String name, int nodeType, String nodeLabel)
        {
            this.nodeName = name;
            this.nodeLabel = nodeLabel;
            this.nodeType = nodeType;
            if (parent != null)
            {
                this.level = parent.getLevel() + 1;
            }
        }

        //public menthods
        public void addChildNode(TreeNode node, string relationLabel)
        {

            if (childNodes == null)
            {
                childNodes = new List<TreeNode>();
            }
            this.childNodes.Add(node);
            node.parent = this;
            node.RelationLabel = relationLabel;
        }

        public int ChildCount()
        {
            return childNodes.Count;
        }

        public TreeNode()
        {
            Random random = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
            int rand = random.Next();
            this.nodeID = rand.ToString();
        }
        public Dictionary<string, TreeNode> listNodes { get; set; }
        public TreeNode normalizeTree()
        {
            this.listNodes = new Dictionary<string, TreeNode>();
            Dictionary<string, TreeNode> eventNodes = new Dictionary<string, TreeNode>();
            Dictionary<string, TreeNode> decisionNodes = new Dictionary<string, TreeNode>();
            Dictionary<string, TreeNode> payOffNodes = new Dictionary<string, TreeNode>();            
            int index = 0;
            TreeNode self = this;
            List<string> nodeQueue = new List<string>();
            this.nodeID = self.nodeID + index.ToString();
            self.Level = 1;
            nodeQueue.Add(self.nodeID);
            listNodes.Add(self.nodeID, self);
            double maxEVM = 0;
            index++;
            while (nodeQueue.Count > 0)
            {
                TreeNode node = listNodes[nodeQueue[0]];
                if (!self.listLevel.ContainsKey(node.Level))
                {
                    self.listLevel.Add(node.Level,1);
                }else
                {
                    self.listLevel[node.Level] = self.listLevel[node.Level] + 1;
                }
                if (node.ChildCount() > 0)
                {
                    foreach (TreeNode item in node.ChildNodes)
                    {
                        if (item.Parent == null)
                        {
                            item.Parent = node;
                        }
                        item.Level = node.Level + 1;                        
                        if (item.payOffState.Count > 0)
                        {
                            double evm = 0;
                            for (int i = 0; i < item.payOffState.Count; i++)
                            {
                                if (node.MaxEventPayOff.Count == 0)
                                {
                                    for(int j = 0; j < item.payOffState.Count; j++)
                                    {
                                        node.MaxEventPayOff.Add(item.payOffState[j]);
                                    }
                                }
                                else
                                {
                                    if (node.MaxEventPayOff[i] < item.payOffState[i])
                                        node.MaxEventPayOff[i] = item.payOffState[i];
                                }
                                evm += item.payOffState[i] * item.priorState[i];
                                TreeNode lastNode = new TreeNode();
                                lastNode.nodeType = (int)define.nodeType.PayOff;
                                lastNode.nodeName = "PayOff " + (i + 1);
                                lastNode.nodeLabel = "PayOff " + (i + 1);
                                lastNode.PayOffValue = item.payOffState[i];
                                item.addChildNode(lastNode, "State " + (i + 1));

                            }
                            item.EMV = evm;
                            if (evm > maxEVM)
                            {
                                maxEVM = evm;
                                self.EMVNode = item;
                            }
                        }                        
                        item.nodeID = item.nodeID + index.ToString();
                        nodeQueue.Add(item.nodeID);
                        listNodes.Add(item.nodeID, item);
                        index++;
                    }
                }
                nodeQueue.RemoveAt(0);
            }
            self.EMV = maxEVM;
            self.listNodes = this.listNodes;           
            return self;
        }


    }
}
