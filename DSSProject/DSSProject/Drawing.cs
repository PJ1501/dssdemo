﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace DSSProject
{
    class DrawingTree
    {
        private Control view;
        private Graphics graphicView;
        private DecisionTree tree;

        private int horizonMargin = 200;
        private int verticalMargin = 200;

        private int celerateX = 0;

        public int HorizonMargin
        {
            get
            {
                return horizonMargin;
            }
            set
            {
                horizonMargin = value;
                //Can do refresh tree graphic here
            }
        }

        public int VerticalMargin
        {
            get
            {
                return verticalMargin;
            }
            set
            {
                verticalMargin = value;
                //Can do refresh tree graphic here
            }
        }


        public DrawingTree(Control view, ref DecisionTree tree)
        {
            this.view = view;
            this.tree = tree;
           
            //Panel scrollSupport = new Panel();
            //scrollSupport.Top = 0;
            //scrollSupport.Left = 0;
            //scrollSupport.Width = 1000;
            //scrollSupport.Height = 1000;
            //scrollSupport.BackColor = Color.Transparent;
            //this.view.Controls.Add(scrollSupport);
            this.graphicView = view.CreateGraphics();
        }

        public void drawTree()
        {
            var rootXStart = view.Width / 2;
            //tree.root.nodeX = 0;
            tree.root = calculateChildsXNode(tree.root);
            tree.root = convertXNodeToXCoor(tree.root);
            drawNode(tree.root);
        }

        private TreeNode convertXNodeToXCoor(TreeNode node)
        {
            var rootXStart = view.Width / 2;
            //node.X = node.nodeX * horizonMargin + view.Width / 2;
            //node.Y = node.getLevel() * verticalMargin + 10;
            foreach (var child in node.ChildNodes)
            {
                convertXNodeToXCoor(child);
            }
            return node;
        }

        private TreeNode calculateChildsXNode(TreeNode node)
        {
            //Console.WriteLine("calculateChildsXNode" +"  "+ node.nodeName+ " x-index:" + node.nodeX);
            //foreach (var child in node.ChildNodes)
            //{
            //    var index = node.ChildNodes.IndexOf(child);
            //    if (node.ChildCount == 1)
            //    {
            //        child.nodeX = child.Parent.nodeX;
            //    }
            //    else if (node.ChildCount == 2)
            //    {                  
            //        if (index == 0) // left of parent                    
            //            child.nodeX = child.Parent.nodeX - 1 ; 
            //        else
            //            child.nodeX = child.Parent.nodeX + 1 ;
            //    }
            //    else
            //    {                    
            //        if (index == 0) // below parent                   
            //            child.nodeX = child.Parent.nodeX;
            //        else if (index % 2 == 0) // below right side 
            //            child.nodeX = child.Parent.nodeX + (index / 2);
            //        else
            //            child.nodeX = child.Parent.nodeX - 1 - (index / 2);
            //    }
            //    celerateX = Math.Min(child.nodeX, celerateX);
            //    calculateChildsXNode(child);
            //}
            return node;
        }

        private Size drawNode(TreeNode node)
        {
            Size parentSize;
            //var text = node.nodeX.ToString();
            //if (node.nodeType == (int)define.nodeType.Decision)
            //{
            //    parentSize = drawDecision(node.X, node.Y, text);
            //}
            //else if (node.nodeType == (int)define.nodeType.Event)
            //{
            //    parentSize = drawEvent(node.X, node.Y, text);
            //}
            //else
            //{
            //    parentSize = drawCost(node.X, node.Y, text);
            //}
            //foreach (var child in node.ChildNodes)
            //{
            //    Size childSize = drawNode(child);
            //    drawLine(node.X + parentSize.Width / 2, node.Y + parentSize.Height, child.X + childSize.Width / 2, child.Y);

            //}
            return new Size();
        }

        public Size drawEvent(int x, int y, string text)
        {
            Rectangle rect = new Rectangle(x, y, 0, 0);
            var size = drawText(rect, text);
            rect.Width = size.Width;
            rect.Height = size.Height;
            graphicView.DrawRectangle(Pens.Black, rect);
            return size;

        }

        public Size drawDecision(int x, int y, string text)
        {
            Rectangle rect = new Rectangle(x, y, 0, 0);
            var size = drawText(rect, text);
            rect.Width = size.Width;
            rect.Height = size.Height;
            graphicView.DrawEllipse(Pens.Black, rect);
            return size;
        }

        private Size drawCost(int x, int y, string text)
        {
            return drawText(new Rectangle(x, y, 0, 0), text);

        }

        private Size drawText(Rectangle rect, string text)
        {
            Font drawFont = new Font("Arial", 15);
            SolidBrush drawBrush = new SolidBrush(Color.Black);
            graphicView.DrawString(text, drawFont, drawBrush, rect.X, rect.Y);
            drawFont.Dispose();
            drawBrush.Dispose();
            return TextRenderer.MeasureText(text, drawFont);
        }

        private void drawLine(int x1, int y1, int x2, int y2)
        {
            var myPen = new Pen(Color.Red);
            graphicView.DrawLine(myPen, x1, y1, x2, y2);
        }


        public void clearGraphic()
        {
            graphicView.Clear(Color.White);
        }

    }
}
