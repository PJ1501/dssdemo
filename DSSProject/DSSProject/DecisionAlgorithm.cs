﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DSSProject
{
    class DecisionAlgorithm
    {
        public List<DecisionAttribute> attributes { get; set; }
        public DecisionTree decisionTree { get; set; }
        public TreeNode treeNode { get; set; }
        public DecisionAlgorithm(List<DecisionAttribute> attributes)
        {
            this.attributes = attributes;
        }
        public DecisionAlgorithm(DecisionTree decisionTree)
        {
            this.decisionTree = decisionTree;
        }
        public DecisionAlgorithm(TreeNode treeNode)
        {
            this.treeNode = treeNode;
        }

        public static String EVM(TreeNode rootNode)
        {
            Dictionary<string, TreeNode> listNodes = new Dictionary<string, TreeNode>();

            List<string> nodeQueue = new List<string>();
            nodeQueue.Add(rootNode.nodeID);
            listNodes.Add(rootNode.nodeID, rootNode);
            String solution = "------Tính EMV-----\n";
            String maxPayoff = "";
            String solutionEPVI = "------Tính EVPI-----\nMax payoff:";
            String EPVIStr = "\n";
            double EPVI = 0;
            while (nodeQueue.Count > 0)
            {
                TreeNode node = listNodes[nodeQueue[0]];
                if (node.ChildCount() > 0)
                {

                    foreach (TreeNode item in node.ChildNodes)
                    {

                        if (item.payOffState.Count > 0)
                        {
                            if (node.priorState == null)
                                node.priorState = item.priorState;
                            solution += item.Description + " EMV = ";
                            for (int i = 0; i < item.payOffState.Count; i++)
                            {
                                if (i < item.payOffState.Count - 1)
                                    solution += item.priorState[i] + "(" + item.payOffState[i] + ")+";
                                else
                                    solution += item.priorState[i] + "(" + item.payOffState[i] + ")=";
                            }
                            solution += item.EMV + "\n";
                        }

                        nodeQueue.Add(item.nodeID);
                        listNodes.Add(item.nodeID, item);

                    }

                    if (node.MaxEventPayOff.Count > 0)
                    {
                        
                        for(int i = 0;i<node.MaxEventPayOff.Count;i++)
                        {
                            EPVI += node.MaxEventPayOff[i] * node.priorState[i];
                            maxPayoff += node.MaxEventPayOff[i]+"\t";
                            EPVIStr += node.priorState[i] + "(" + node.MaxEventPayOff[i] + ")";
                            if (i < node.MaxEventPayOff.Count - 1)
                                EPVIStr += "+";
                            else EPVIStr += "- "+node.EMV+" = ";
                        }
                        EPVI -= node.EMV;
                    }
                }
                nodeQueue.RemoveAt(0);
            }
            solution += "Chọn giá trị EMV lớn nhất => chọn " + rootNode.EMVNode.nodeName;
            solution += "\n";
            solution += solutionEPVI+maxPayoff+ EPVIStr;
            solution += EPVI;
            return solution;

        }

        public static void createTableEVM(TreeNode rootNode, TabControl tabcontrol)
        {
            DataGridView evm = new DataGridView();
            //evm.Columns.Add("decision", "Decision");
            //evm.Cl
            evm.Columns.Add("Decision", "Decision");
            var columns = rootNode.EMVNode.Parent.ChildNodes.Count;
            foreach(TreeNode node in rootNode.EMVNode.Parent.ChildNodes)
            {
                evm.Columns.Add(node.nodeName, node.nodeName);
            }
            evm.Columns.Add("expecteded","Expected");
            evm.Columns.Add("ecommended","Recommended");
            foreach (TreeNode node in rootNode.EMVNode.Parent.ChildNodes)
            {
                String[] s = new String[rootNode.EMVNode.payOffState.Count];
                for(int i = 0;i<)
                evm.Rows.Add(new String[]{"1","2","3" });
            }
            evm.Dock = DockStyle.Fill;
            tabcontrol.TabPages[0].Controls.Add(evm);
        }

        
    }
}
