﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSSProject
{
    class DecisionAttribute
    {
        public String alternative { get; set; }
        public Double maxPayOff { get; set; }
        public Double minPayOff { get; set; }
        public List<State> stateNature { get; set; }        
        public DecisionAttribute(String alternative, List<State> states)
        {
            this.alternative = alternative;        
            this.stateNature = states;
            if (this.stateNature.Count > 0)
            {
                
                foreach(State item in this.stateNature)
                {
                    if(item.stateValue > maxPayOff)
                    {
                        maxPayOff = item.stateValue;
                    }
                    if(item.stateValue < minPayOff)
                    {
                        minPayOff = item.stateValue;
                    }
                }
            }
        }
        
    }
}
