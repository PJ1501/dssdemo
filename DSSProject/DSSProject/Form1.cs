﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DSSProject
{
    public partial class Form1 : Form
    {
        enum decisionType { EMV, EVPI, BAYE, KHONG_XS }
        enum approachType { LAC_QUAN, BAO_THU, HOI_TIEC }
        public int quyetDinh = (int)decisionType.EMV; // 0: EMV , 1: EVPI 2: Baye's
        public int tiepCan = (int)approachType.LAC_QUAN; //0: lac quan, 1: bao thu, 2: hoi tiec
        BindingSource bsDecision = new BindingSource();
        BindingSource bsEvent = new BindingSource();
        ObservableCollection<TreeNode> listNode = new ObservableCollection<TreeNode>();
        DecisionTree decisionTree;
        private RadioButton selectedrb;
        public Form1()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            comboBox1.SelectedIndex = (int)decisionType.EMV;
            comboBox2.SelectedIndex = (int)approachType.LAC_QUAN;
            decisionTree = new DecisionTree();
            bsDecision.DataSource = typeof(TreeNode);
            bsEvent.DataSource = typeof(TreeNode);
            cbDecision.DataSource = bsDecision;
            cbEvent.DataSource = bsEvent;
            selectedrb = rbEvent;
            
        }

        private void Decision_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
        /**
        * Load file
        */
        private void loadData(object sender, EventArgs e)
        {
            OpenFileDialog OpenDiag = new OpenFileDialog();
            OpenDiag.InitialDirectory = Application.StartupPath;
            OpenDiag.DefaultExt = ".txt";
            OpenDiag.Filter = "Text documents (.txt)|*.txt";
            //dataTable.Rows.Clear();
            //dataTable.Columns.Clear();

            if (OpenDiag.ShowDialog() == DialogResult.OK)
            {

            }
        }
        public void loadDataofBaye()
        {

        }
        public void loadDataNormal(String path)
        {
            StreamReader sr = new StreamReader(path);
            string line = "";
            int indexLine = 1;
            while ((line = sr.ReadLine()) != null)
            {
                line = line.Trim();
                string[] value = line.Trim().ToLower().Split('\t').ToArray();
                if (indexLine == 1)
                {
                    for (int i = 0; i < value.Length; i++)
                    {
                        DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
                        column.HeaderText = value[i].ToUpper();
                        column.Name = value[i].ToUpper();
                        //dataTable.Columns.Add(column);
                    }
                }
                else if (indexLine == 2)
                {
                    for (int i = 0; i < value.Length; i++)
                    {
                        if (value[0].Equals("Probability"))
                        {

                        }
                    }
                }
                indexLine++;

            }
            sr.Close();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == (int)decisionType.KHONG_XS)
            {
                comboBox2.Enabled = true;
            }
            else
            {
                comboBox2.Enabled = false;
            }
        }
        public void quyetDinhXacSuat()
        {

        }
        public void quyetDinhLacQuan()
        {

        }
        public void quyetDinhBaoThu()
        {

        }
        public void quyetDinhHoiTiec()
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }


        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb == null)
            {
                MessageBox.Show("Sender is not a RadioButton");
                return;
            }
            // Ensure that the RadioButton.Checked property
            // changed to true.
            if (rb.Checked)
            {
                // Keep track of the selected RadioButton by saving a reference
                // to it.
                selectedrb = rb;
                int type = int.Parse(selectedrb.Tag.ToString());
                if (type == (int)define.nodeType.Decision)
                {
                    cbEvent.Enabled = true;
                    cbDecision.Enabled = false;
                }else if(type == (int)define.nodeType.Event)
                {
                    cbDecision.Enabled = true;
                    cbEvent.Enabled = false;
                }
            }
        }

        private TreeNode selectedNode;
        private void listEventParent_changed(object sender, EventArgs e)
        {            
            selectedNode = (TreeNode)cbEvent.SelectedItem;
            
        }
        public void createNode(object sender, EventArgs e)
        {
            Random random = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
            int rand = random.Next();
            int type = int.Parse(selectedrb.Tag.ToString());
            TreeNode node = new TreeNode("node_" + rand.ToString(), type, nodeLabel.Text);
            if (decisionTree.root == null)
            {
                selectedNode = node;
            }
            if (selectedNode != null)
            {
                node.Parent = selectedNode;
            }
            if (!relationLabel.Text.ToString().Equals(""))
            {
                selectedNode.addChildNode(node, relationLabel.Text.ToString());
            }
            decisionTree.addNode(node);

            if (node.nodeType == (int)define.nodeType.Decision)
            {
                bsDecision.Add(node);
            }
            else if (node.nodeType == (int)define.nodeType.Event)
            {
                bsEvent.Add(node);
            }
        }
        public void loadDataFromFile(object sender, EventArgs e)
        {
            OpenFileDialog OpenDiag = new OpenFileDialog();
            OpenDiag.InitialDirectory = Application.StartupPath;
            OpenDiag.DefaultExt = ".json";
            OpenDiag.Filter = "Json documents (.json)|*.json";
            //dataTable.Rows.Clear();
            //dataTable.Columns.Clear();

            if (OpenDiag.ShowDialog() == DialogResult.OK)
            {
                StreamReader sr = new StreamReader(OpenDiag.FileName);
                string line = "";
                string dataStr = "";     
                while ((line = sr.ReadLine()) != null)
                {
                    dataStr += line.Trim();
                }
                sr.Close();
                try {
                    TreeNode rootNode = JsonConvert.DeserializeObject<TreeNode>(dataStr);
                    TreeNode node = rootNode.normalizeTree();
                    MyDrawing myDrawn = new MyDrawing(treePanel, node);
                    myDrawn.DrawnTree();
                    String solutionStr = DecisionAlgorithm.EVM(node);
                    solution.Text = solutionStr;
                    DecisionAlgorithm.createTableEVM(node, tabControl);
                }catch(Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            
        }
    }
}
