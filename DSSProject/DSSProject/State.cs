﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSSProject
{
    class State
    {
        public String stateName { get; set; }
        public Double stateValue { get; set; }
        public Double probability { get; set; }
        public State(String name, Double value)
        {
            this.stateName = name;
            this.stateValue = value;
        }
        public State(String name, Double value, Double propability)
        {
            this.stateName = name;
            this.stateValue = value;
            this.probability = probability;
        }
    }
}
