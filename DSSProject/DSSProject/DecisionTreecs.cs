﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DSSProject
{
    class DecisionTree
    {
        public TreeNode root { get; set; }
        public int[] level;
        public Dictionary<String, TreeNode> listNode { get; set; }
        private BindingSource decisionNodes;
        private BindingSource eventNodes;
        private BindingSource payOffNodes;
        public DecisionTree(TreeNode root)
        {
            this.root = root;
        }
        public DecisionTree()
        {

        }
        public void addRoot(ref TreeNode node)
        {
            this.root = node;
        }
        public void addNode(TreeNode node)
        {
            if (node != null)
            {
                if (this.root == null)
                {
                    this.root = node;
                }
                if (listNode == null)
                {
                    listNode = new Dictionary<string, TreeNode>();
                }
                this.listNode.Add(node.nodeName, node);
                if (node.nodeType == (int)define.nodeType.Decision)
                {
                    adDecisionNode(ref node);
                }
                else if (node.nodeType == (int)define.nodeType.Event)
                {
                    addEventNode(ref node);
                }
                else
                {
                    addPayOffNode(ref node);
                }
            }
        }
        public void adDecisionNode(ref TreeNode node)
        {
            if (decisionNodes == null)
                decisionNodes = new BindingSource();
            decisionNodes.Add(node);
        }
        public void addEventNode(ref TreeNode node)
        {
            if (eventNodes == null)
            {
                eventNodes = new BindingSource();
            }
            eventNodes.Add(node);
        }

        public void addPayOffNode(ref TreeNode node)
        {
            if (payOffNodes == null)
            {
                payOffNodes = new BindingSource();
            }
            payOffNodes.Add(node);
        }
        public BindingSource listDecision()
        {
            if (decisionNodes != null)
                return decisionNodes;
            return null;
        }
        public BindingSource listEvent()
        {
            if (eventNodes != null)
                return eventNodes;
            return null;
        }
        public BindingSource listPayOff()
        {
            return payOffNodes;
        }                
    }
}
